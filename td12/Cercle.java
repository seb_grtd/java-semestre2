import javafx.scene.shape.Circle;
import java.util.List;
import javafx.scene.paint.Color;

public class Cercle extends Circle{
    private static double rayonMini = 30.0;

    public Cercle(double largeur, double hauteur){
        super(Math.random()*largeur, Math.random()*hauteur, rayonMini);
        super.setFill(new Color(Math.random(),Math.random(),Math.random(),1.0));
    }

    public Cercle(List<Cercle> list, double largeur , double hauteur){
        super(Math.random()*largeur, Math.random()*hauteur, rayonMini);
        super.setFill(new Color(Math.random(),Math.random(),Math.random(),1.0));
        this.placerAuHasard(list, largeur, hauteur);
    }

    private boolean intersecte(Cercle c){
        double distance = Math.sqrt(Math.pow(this.getCenterX()-c.getCenterX(), 2)+Math.pow(this.getCenterY()-c.getCenterY(), 2));
        return distance < this.getRadius()+c.getRadius();
    }

    private boolean estDansLeCadre(double largeur, double hauteur){
        double xNotreCercle = this.getCenterX();
        double yNotreCercle = this.getCenterY();
        double rayonNotreCercle = this.getRadius();
        
        return xNotreCercle-rayonNotreCercle >= 0 && xNotreCercle+rayonNotreCercle <= largeur && yNotreCercle-rayonNotreCercle >=0 && yNotreCercle+rayonNotreCercle <= hauteur; 
    }

   private boolean estValide(List<Cercle> liste, double largeur, double hauteur){
        for (Cercle c : liste){
            if (this.intersecte(c) || !this.estDansLeCadre(largeur, hauteur)){
                return false;
            }
        }
        return true;
    }

    private void placerAuHasard(List<Cercle> list, double largeur, double hauteur) {
        double rayon = rayonMini;
        while (!this.estValide(list, largeur, hauteur) && rayon >= 0.1) {
            this.setCenterX(Math.random() * largeur);
            this.setCenterY(Math.random() * hauteur);
            this.setRadius(rayon);
            rayon -= 0.1;
        }
        list.add(this);
    }

    public void grossir(List<Cercle> liste, double largeur, double hauteur){
        while (this.estValide(liste, largeur, hauteur)){
            this.setRadius(this.getRadius()+0.1);
        }
    }
}