import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Executable{
    public static void main(String[] args){
        Sorcier test = new Sorcier("test",1,2);
        assert test.getNom().equals("test");
        assert test.getCourage() == 1;
        assert test.getSagesse() == 2;
        assert !test.estCourageux();

        Sorcier test1 = new Sorcier("test",10,2);
        assert test1.estCourageux();


        Maison serpentar = new Maison("Serpentar");
        serpentar.ajouter("Adrian",9,7);
        serpentar.ajouter("Drago",6,5);
        serpentar.ajouter("Pansy",4,10);
        serpentar.ajouter("Gregory", 6, 7);
        assert serpentar.nombreEleve() == 4;
        assert serpentar.contientCourageux();
        assert serpentar.leMoinsCourageux().equals(new Sorcier("Pansy",4,10));
        assert serpentar.lePlusSage().equals(new Sorcier("Pansy",4,10));

        List<Sorcier> serpentarTrieParCourage = Arrays.asList(new Sorcier("Pansy",4,10), new Sorcier("Drago",6,5), 
        new Sorcier("Gregory", 6, 7), new Sorcier("Adrian",9,7));
        serpentar.trieParCourage();
        assert serpentar.getEleves().equals(serpentarTrieParCourage);

        Maison griffondor = new Maison("Griffondor");
        griffondor.ajouter("Hermione",8,6);
        griffondor.ajouter("Neuville",10,4);
        griffondor.ajouter("Dean",9,4);
        assert griffondor.nombreEleve() == 3;
        assert griffondor.leMoinsCourageux().equals(new Sorcier("Hermione",8,6));
        assert griffondor.lePlusSage().equals(new Sorcier("Hermione",8,6));

        List<Sorcier> griffondorTrieParCourage = Arrays.asList(new Sorcier("Hermione",8,6), new Sorcier("Dean",9,4),
        new Sorcier("Neuville",10,4));
        griffondor.trieParCourage();
        assert griffondor.getEleves().equals(griffondorTrieParCourage);
        

        Maison serdaigle = new Maison("Serdaigle");
        serdaigle.ajouter("Luna",2,9);
        serdaigle.ajouter("Gilderoy",7,9);
        assert !serdaigle.contientCourageux();

        Maison poufsouffle = new Maison("Poufsouffle");
        poufsouffle.ajouter("Norbert",3,7);

        Ecole poudlard = new Ecole("Poudlard");
        poudlard.ajouter(poufsouffle);
        poudlard.ajouter(griffondor);
        poudlard.ajouter(serpentar);
        poudlard.ajouter(serdaigle);

        assert poudlard.plusGrandeMaison().equals(serpentar);

        List<Sorcier> sorciersCourageux = Arrays.asList(new Sorcier("Dean", 9, 4), new Sorcier("Neuville", 10, 4), new Sorcier("Adrian", 9, 7));
        assert poudlard.lesCourageux().equals(sorciersCourageux);

        List<Sorcier> sorciersTries = Arrays.asList(new Sorcier("Luna", 2 ,9), new Sorcier("Norbert",3,7), 
        new Sorcier("Pansy", 4, 10), new Sorcier("Drago", 6, 5), new Sorcier("Gregory", 6, 7),
        new Sorcier("Gilderoy", 7, 9), new Sorcier("Hermione", 8, 6), new Sorcier("Dean", 9, 4), new Sorcier("Adrian", 9, 7), new Sorcier("Neuville",10,4));

        assert poudlard.elevesTrieParCourage().equals(sorciersTries);
    }
}