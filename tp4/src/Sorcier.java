public class Sorcier implements Comparable<Sorcier> {
    private String nom;
    private int courage;
    private int sagesse;

    /** 
     * 
     * @param nom
     * @param courage
     * @param sagesse
     */
    public Sorcier(String nom, int courage, int sagesse){
        this.nom = nom;
        this.courage = courage;
        this.sagesse = sagesse;
    }

    public String getNom() {
        return nom;
    }

    public int getCourage() {
        return courage;
    }

    public int getSagesse() {
        return sagesse;
    }

    public boolean estCourageux(){
        return this.courage > 8;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){ return false; }
        if (obj == this){ return true; }
        if (obj instanceof Sorcier){
            Sorcier src = (Sorcier) obj;
            return src.nom.equals(this.nom) && src.courage == this.courage && this.sagesse == src.sagesse;
        }
        return false;
    }

    @Override
    public int compareTo(Sorcier s) {
        if (this.courage == s.courage){
            return this.sagesse - s.sagesse;
        }
        return this.courage-s.courage;
    }

    @Override
    public String toString(){
        return this.nom + " courage: " + this.courage + " sagesse: " + this.sagesse;
    }
}
