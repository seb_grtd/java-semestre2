import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Maison implements Comparable<Maison>{
    private String nom;
    List<Sorcier> eleves;

    public Maison(String nomMaison){
        this.nom = nomMaison;
        this.eleves = new ArrayList<>();
    }

    public List<Sorcier> getEleves(){
        return this.eleves;
    }

    public boolean ajouter(String nomSorcier, int courage, int sagesse){
        Sorcier sorcierAAjouter = new Sorcier(nomSorcier, courage, sagesse);
        if (this.eleves.contains(sorcierAAjouter)){
            return false;
        }
        this.eleves.add(new Sorcier(nomSorcier, courage, sagesse));
        return true;
    }
    
    public int nombreEleve(){
        return this.eleves.size();
    }

    public boolean contientCourageux(){
        for (Sorcier s : this.eleves){
            if(s.estCourageux()){
                return true;
            }
        }
        return false;
    }

    public Sorcier leMoinsCourageux(){
        int courageMin = 0;
        Sorcier sorcierMoinsCourageux = null;
        for (Sorcier s: this.eleves){
            if (sorcierMoinsCourageux == null || courageMin > s.getCourage()){
                courageMin = s.getCourage();
                sorcierMoinsCourageux = s;
            }
        }
        return sorcierMoinsCourageux;
    }

    public Sorcier lePlusSage(){
        int sagesseMax = 0;
        Sorcier sorcierPlusSage = null;
        for (Sorcier s : this.eleves){
            if (sorcierPlusSage == null || sagesseMax < s.getSagesse()){
                sagesseMax = s.getSagesse();
                sorcierPlusSage = s;
            }
        }
        return sorcierPlusSage;
    }

    public void trieParCourage(){
        Collections.sort(this.eleves);
    }

    @Override
    public int compareTo(Maison m) {
        return this.nombreEleve()-m.nombreEleve();
    }
}
