import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Ecole {
    private String nom;
    private List<Maison> maisons;

    public Ecole(String nomEcole){
        this.nom = nomEcole;
        maisons = new ArrayList<>();
    }

    public void ajouter(Maison maison){
        this.maisons.add(maison);
    }

    public Maison plusGrandeMaison(){
        return Collections.max(this.maisons);
    }

    public List<Sorcier> eleves(){
        List<Sorcier> res = new ArrayList<>();
        for (Maison m : maisons){
            for(Sorcier s : m.eleves){
                res.add(s);
            }
        }
        return res;
    }

    public List<Sorcier> lesCourageux(){
        List<Sorcier> res = new ArrayList<>();
        for (Maison m : maisons){
            for(Sorcier s : m.eleves){
                if (s.estCourageux()){
                    res.add(s);
                }
            }
        }
        return res;
    }

    public List<Sorcier> elevesTrieParCourage(){
        List <Sorcier> res = new ArrayList<>();
        Maison maisonTemp = new Maison("temp");
        for(Sorcier s : this.eleves()){
            maisonTemp.ajouter(s.getNom(), s.getCourage(), s.getSagesse());
        }
        maisonTemp.trieParCourage();
        for (Sorcier s : maisonTemp.eleves){
            res.add(s);
        }
        return res;
    }
}
