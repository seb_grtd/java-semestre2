public class Personnage{
    private String nom;
    private int tailleBarbe;
    private int tailleOreilles;

    public Personnage(String nom, int barbe, int tailleOreilles){
        this.nom=nom;
        this.tailleBarbe = barbe;
        this.tailleOreilles=tailleOreilles;
    }

    public String getNom(){
        return this.nom;
    }

    public int getBarbe(){
        return this.tailleBarbe;
    }

    public int getTailleOreilles(){
        return this.tailleOreilles;
    }

    @Override

    public String toString(){
        return "nom: "+this.nom+" barbe: "+this.tailleBarbe+" oreilles: "+this.tailleOreilles;
    }
}