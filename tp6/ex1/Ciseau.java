public class Ciseau extends Coupant implements Outils{
    public Ciseau(String marque){ 
        super(marque);
    }
    
    public void utiliser() {
        System.out.print ("mes ciseaux " + marque + " permettent de " ) ;
        super.couper();
        System.out.println();
    }
}
