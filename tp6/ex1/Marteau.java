public class Marteau implements Outils {
    private String marque;

    public Marteau(String m){ 
        this.marque = m; 
    }

    public void taper(){ 
        System.out.print(" taper");
    }

    public void utiliser() {
        System.out.print("mon marteau " + this.marque + " permet de");
        this.taper();
        System.out.println();
    }
}
