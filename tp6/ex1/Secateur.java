public class Secateur extends Coupant implements Outils {

    public Secateur(String marque){
        super(marque);
    }


    public void utiliser(){
        System.out.print("mon sé cateur " + marque + " permet de ") ;
        super.couper();
        System.out.println();
    }
}