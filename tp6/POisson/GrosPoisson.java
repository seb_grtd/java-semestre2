public class GrosPoisson extends Poisson implements Dessinable{
    public GrosPoisson(int posX, int posY, double vitesseX){
        super(posX, posY, vitesseX);
    }

    public Dessin getDessin(){
        Dessin dessin = new Dessin();
        String couleur = "0x000000";
        dessin.ajouteChaine(posX, posY, "; ,//; , ,;/", couleur);
        dessin.ajouteChaine(posX, posY-1, "o :::::::;;///", couleur);
        dessin.ajouteChaine(posX, posY-2, ">::::::::;;///", couleur);
        dessin.ajouteChaine(posX, posY-3, "’ ’ ////// ’' ’ ;/", couleur);
        super.setDessin(dessin);
        return dessin;
    }
}
