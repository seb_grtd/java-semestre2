public class PetitPoisson extends Poisson{


    public PetitPoisson(int posX, int posY, double vitesseX){
        super(posX, posY, vitesseX);
    }

    public Dessin getDessin(){
        Dessin dessin = new Dessin();
        String couleur = "0x000000";
        if (super.direction==1){
            dessin.ajouteChaine(posX, posY, "<><", couleur);
        }
        else{
            dessin.ajouteChaine(posX, posY, "><>", couleur);
        }
        super.setDessin(dessin);
        return dessin;
    }
}
