import java.util.List;
import java.util.ArrayList;

public class Aquarium implements Dessinable{
    int hauteur;
    int largeur;
    List<ElementsMarins> elemMarins; 

    public Aquarium(){
        this.hauteur = 50;
        this.largeur = 120;
        this.elemMarins = new ArrayList<>();
        elemMarins.add(new PetitPoisson(20,20,-2.0));
        elemMarins.add(new GrosPoisson(20,40,1.0));
        elemMarins.add(new Algue(30));
        elemMarins.add(new Bulle(30,20, 10.0));
    }

    public int getHauteur() {
        return hauteur;
    }
    
    public int getLargeur() {
        return largeur;
    }

    public void ajouter(ElementsMarins el){
        elemMarins.add(el);
    }

    public Dessin getDessin() {
        Dessin dessin = new Dessin();
        for (ElementsMarins el : this.elemMarins){
            dessin.union(el.getDessin());
        }
        return dessin;
    }

    public void evolue(){
        for (ElementsMarins el : this.elemMarins){
            if( el instanceof Poisson){
                if (el instanceof PetitPoisson){
                    if (el.getPosX()+3 >= this.largeur || el.getPosX() <= 0){
                        ((Poisson) el).changeDirection();
                    }
                }
                ((Poisson) el).nage();
            }
            else if (el instanceof Algue){
                ((Algue) el).ondule();
            }
            else{
                ((Bulle) el).remonte();
            }
        }
    }
}
