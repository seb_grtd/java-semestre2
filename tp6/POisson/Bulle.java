import javax.management.modelmbean.DescriptorSupport;

public class Bulle extends ElementsMarins {
    double vitesse;

    public Bulle(int posX, int posY, double vitesse){
        super(posX, posY);
        this.vitesse = vitesse;
    }

    public Dessin getDessin(){
        Dessin dessin = new Dessin();
        dessin.ajouteChaine(posX, posY, "o", "0x000000");
        super.setDessin(dessin);
        return dessin;
    }

    public void remonte(){
        
    }
}
