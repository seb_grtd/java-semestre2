public interface Dessinable {
    public Dessin getDessin();
}
