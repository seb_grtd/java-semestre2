public class Algue extends ElementsMarins{
    private boolean estVersGauche;
    public Algue(int posX){
        super(posX, 0);
    }

    public void dessinGauche(){
        Dessin dessin = new Dessin();
        dessin.ajouteChaine(super.posX, 0, "(", "0x000000");
        dessin.ajouteChaine(super.posX, 1, ")", "0x000000");
        dessin.ajouteChaine(super.posX, 2, "(", "0x000000");
        dessin.ajouteChaine(super.posX, 3, ")", "0x000000");
        dessin.ajouteChaine(super.posX, 4, "(", "0x000000");
        super.setDessin(dessin);
    }

    public void dessinDroite(){
        Dessin dessin = new Dessin();
        dessin.ajouteChaine(super.posX, 0, ")", "0x000000");
        dessin.ajouteChaine(super.posX, 1, "(", "0x000000");
        dessin.ajouteChaine(super.posX, 2, ")", "0x000000");
        dessin.ajouteChaine(super.posX, 3, "(", "0x000000");
        dessin.ajouteChaine(super.posX, 4, ")", "0x000000");
        super.setDessin(dessin);
    }

    public void ondule(){
        if (estVersGauche){
            dessinGauche();
        }
        else{
            dessinDroite();
        }
        estVersGauche = !estVersGauche;
    }

} 