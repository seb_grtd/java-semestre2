public class Poisson extends ElementsMarins {
    double vitesseX;
    protected int direction;

    public Poisson(int posX, int posY, double vitesseX){
        super(posX, posY);
        this.vitesseX = vitesseX;
        this.direction = 1;
    }

    public void changeDirection(){
        this.direction = this.direction*-1;
    }

    public void nage(){
        this.posX+=((int)this.vitesseX)*this.direction;
    }
}
