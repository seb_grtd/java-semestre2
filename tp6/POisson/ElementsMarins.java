public class ElementsMarins implements Dessinable{
    protected int posX;
    protected int posY;
    protected Dessin dessinElem;

    public ElementsMarins(int posX, int posY){
        this.posX = posX;
        this.posY = posY;
        this.dessinElem = new Dessin();
    }

    public void setDessin(Dessin dessin){
        this.dessinElem = dessin;
    }

    public int getPosX(){
        return this.posX;
    }

    public int getPosY(){
        return this.posY;
    }

    public Dessin getDessin(){
        return this.dessinElem;
    }
}


