import java.util.List;
import java.util.ArrayList;

public class Pluviometrie {
    public int annee;
    public int semaine;
    public List<Integer> precipitations;

    public Pluviometrie(int annee, int semaine){
        this.annee = annee;
        this.semaine = semaine;
        this.precipitations = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            this.precipitations.add(null);
        }
    }

    public void setPrecipitation(int jour, Integer pluie){
        this.precipitations.set(jour, pluie);
    }

    public Integer getPluie(int jour){
        return this.precipitations.get(jour);
    } 

    public int quantiteTotale(){
        int res = 0;
        for(Integer pluie : this.precipitations){
            if(pluie != null)
                res += pluie.intValue();
        } 
        return res;
    }

    public int quantiteMax(){
        int qteMax = this.precipitations.get(0).intValue();
        for(int i = 1; i < this.precipitations.size(); i++){
            if (this.precipitations.get(i) != null && qteMax < this.precipitations.get(i))
                qteMax = this.precipitations.get(i).intValue();
        }
        return qteMax;
    }

    public boolean estPluvieuse(){
        for(int i = 1; i < this.precipitations.size(); i++){
            if (this.precipitations.get(i-1) != null && this.precipitations.get(i) != null)
            return true;
        }
        return false;
    }
}
