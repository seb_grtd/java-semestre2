public class Bouteille {
    private String region;
    private String appelation;
    private int annee;

    public Bouteille(String region, String appelation, int annee){
        this.region = region;
        this.appelation = appelation;
        this.annee = annee;
    }

    public String getRegion() {
        return this.region;
    }

    public String getAppelation() {
        return this.appelation;
    }

    public int getMillesime() {
        return this.annee;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        if (obj == this) { return true;} ;
        if (obj == null) {return false;};
        if (obj instanceof Bouteille){
            Bouteille b = (Bouteille) obj;
            return b.getAppelation().equals(this.getAppelation())
            && b.getRegion().equals(this.getRegion()) && b.getMillesime() == this.getMillesime();
        }
        return false;
    }
}
