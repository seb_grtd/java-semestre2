import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class ExecutableCave {
    public static void main(String[] args){
        Cave maCave = new Cave();
        maCave.ajouteBouteille("Bordeaux", "Pomerol", 2005);
        maCave.ajouteBouteille("Bordeaux","Pomerol",2007);
        maCave.ajouteBouteille("Bourgogne","Nuits St George", 2001);
        maCave.ajouteBouteille("Savoie", "Pinot Noir", 2012);
        maCave.ajouteBouteille("Bordeaux", "Pomerol", 2007);
        maCave.ajouteBouteille("Loire", "Chinon", 2017);

        assert 6 == maCave.nbBouteille();
        assert 3 == maCave.nbBouteillesDeRegion("Bordeaux");

        Bouteille b = maCave.plusVieilleBouteille();

        assert "Nuits St George".equals(b.getAppelation());

        assert maCave.contient("Bordeaux", "Pomerol", 2007);
        assert !maCave.contient("Bordeaux","Pomerol",2003);
    }
}
