import java.util.List;
import java.util.ArrayList;

public class Cave {
    private List<Bouteille> bouteilles;

    public Cave(){
        bouteilles = new ArrayList<>();
    }

    public void ajouteBouteille(String region, String Appelation, int Millesime){
        bouteilles.add(new Bouteille(region, Appelation, Millesime));
    }

    public int nbBouteille(){
        return bouteilles.size();
    }

    public int nbBouteillesDeRegion(String region){
        int compteur = 0;
        for (Bouteille b : bouteilles){
            if (b.getRegion() == region)
                compteur += 1;
        }
        return compteur;
    }

    public Bouteille plusVieilleBouteille(){
        int ageMin = bouteilles.get(0).getMillesime();
        Bouteille bouteillePlusVieille = bouteilles.get(0);

        for (int i = 1; i < bouteilles.size(); i++){
            if (bouteilles.get(i).getMillesime() < ageMin){
                ageMin = bouteilles.get(i).getMillesime();
                bouteillePlusVieille = bouteilles.get(i);
            }
        }
        return bouteillePlusVieille;
    }

    public boolean contient(String region, String Appelation, int Millesime){
        return bouteilles.contains(new Bouteille(region, Appelation, Millesime));
    }
}
