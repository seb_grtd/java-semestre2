import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class T9{
    private HashMap<String,Double> dicoFreq;
    private HashMap<String, String> PrefixeMot;
    /**
      * @param mot
      * une chaine de caractère
      * @return une chaine de caractères correspondant à la suite de touches à taper pour obtenir le mot en entrée.
      *  Par exemple pour le mot 'toto', la fonction renvoie '8686'
      */
    public static String toTouches(String mot){
        // TODO Q3 : à concevoir
        String touches = "";
        for (int i = 0; i< mot.length(); i++)
            for (int j = 1; j <= 9; j++){
                if (getTexte(j).indexOf(mot.charAt(i)) > 0){
                    touches += j;
                }
            }
        return touches;
    }
    /**
     * Constructeur
     */
    T9()
    {
        // TODO Q4 : initialiser et remplir dicoFreq
        dicoFreq = new HashMap<>();
        PrefixeMot = new HashMap<>();
        try{
            Scanner in = new Scanner(new FileReader("liste_mots.txt")).useLocale(Locale.US);
            while(in.hasNext()){
                String s=in.next();
                double d=in.nextDouble();
                dicoFreq.put(s,d);
            }
        }
        catch(FileNotFoundException e){
            System.out.println("Pas de fichier");
        }
    }
    /**
      * @param prefixe
      *     une chaine de caractères correspondant à une suite de touches tapées, par exemple "8686"
      * @return le mot le plus probable correspondant à la suite de touches.
      */
    public String getMot(String prefixe){
        // TODO : à concevoir
        String motPlusProbable = "";
        double probabiliteMot = 0.0;
        String mot = "";
        String texteBrut = "";
        int nbPossibilites = 1;
        List<String> possibilites = new ArrayList<>();
        for (int i = 0; i<prefixe.length(); i++){
            char touche = prefixe.charAt(i);
            if (touche != '#' && touche != '0'){
                int numTouche = Character.getNumericValue(touche)-1;
                String texteCorrespondant = getTexte(numTouche);
                possibilites.add(texteCorrespondant);
                nbPossibilites *= texteCorrespondant.length();
            }
        }

        List<String> mots = new ArrayList<>();

        for (int i = 0; i < nbPossibilites; i++){
            mots.add("");
        }

        System.out.println(nbPossibilites);

        for (int i = 0; i < possibilites.size(); i++){
            String lettresPossibles = possibilites.get(i);
            for (int j = 0 ; j < lettresPossibles.length(); j++){
                for (int k = 0; k < nbPossibilites/lettresPossibles.length(); k++){
                    int position = (k+(nbPossibilites/lettresPossibles.length())*j+i*(lettresPossibles.length())*k+j)%nbPossibilites;
                    if (i == 0){
                        position = k+(nbPossibilites/lettresPossibles.length())*j;
                    }
                    mots.set(position, mots.get(position)+Character.toString(lettresPossibles.charAt(j)));
                }
            }
        }
        
        for (String m : mots){
            System.out.println(m);
        }
        return prefixe;
    }
    /**
      * @param numeroTouche
      *      un entier correspondant à la position de la touche sur le clavier (par exemple la touche '2' est à la position 1.)
      * @return le symbole qui doit être affiché sur la touche. Par exemple, sur l'entrée 1, le symbole est '2', sur l'entrée 9, le symbole est '*'
    */
    public String getSymbole(int numeroTouche){
        // TODO Q1.2 et Q1.3
        return "" + (numeroTouche < 9 ? numeroTouche+1 : numeroTouche == 9 ? "*" : numeroTouche == 10 ? "0+" : "#");
    }

    /**
      * @param numeroTouche
      *      un entier correspondant à la position de la touche sur le clavier (par exemple la touche '2' est à la position 1.)
      * @return les lettres qui correspondent à la touche. Par exemple, sur l'entrée 1, la fonction renvoie "abc" qui correpond au texte de la touche 2.
    */

    public static String getTexte(int numeroTouche){
        // TODO Q1.4
        // String alphabet = "abcdefghijklmnopqrstuvwxyz";
        // return alphabet.substring((numeroTouche*27)%9, ((numeroTouche*27)%9)+3);
        switch (numeroTouche){
            case 1:
                return "abc";
            case 2:
                return "def";
            case 3:
                return "ghi";
            case 4:
                return "jkl";
            case 5:
                return "mno";
            case 6:
                return "pqrs";
            case 7: 
                return "tuv";
            case 8:
                return "wxyz";
        }
        return "";
    }




}
