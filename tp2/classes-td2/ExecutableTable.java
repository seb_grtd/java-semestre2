public class ExecutableTable {
    public static void main(String [] args) {
        Table diner = new Table();
        diner.ajouteConvive("Albert", 25);
        diner.ajouteConvive("Belle", 85);
        diner.ajouteConvive("Charles", 63);
        diner.ajouteConvive("David", 2);
        diner.ajouteConvive("Elise", 16);
        diner.ajouteConvive("Franck", 47);
        assert diner.sontACote("Albert", "Belle");
        assert !diner.sontACote("Elise", "Albert");
        assert diner.sontACote("Elise", "Franck");
        assert diner.sontACote("Belle", "Charles");
        assert diner.sontACote("Charles", "David");
        assert diner.sontACote("David", "Elise");
        assert diner.lePlusJeune().equals("David");
        System.out.println("age moyen calculé manuellement:"+(25+85+63+2+16+47)/6);
        System.out.println("age moyen calculé avec la fonction: "+diner.ageMoyen());
        diner.ajouteConvive("Patricia", 1);
        assert diner.lePlusJeune().equals("Patricia");
        assert diner.nombreDAdultes() == 4;
        diner.ajouteConvive("Patricia Pro Max", 20);
        assert diner.nombreDAdultes() == 5;
        diner.echange("Albert", "David");    
        assert !diner.sontACote("Albert", "Belle");
        assert diner.sontACote("David", "Belle");
        assert diner.sontACote("Albert", "Elise");
        System.out.println("age moyen calculé manuellement:"+(25+85+63+2+16+47+1+20)/8);
        System.out.println("age moyen calculé avec la fonction: "+diner.ageMoyen());
    }
}
