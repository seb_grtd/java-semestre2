import java.util.List;
import java.util.ArrayList;

public class Table{

    private List<Personne> lesConvives;

    public Table(){
        this.lesConvives = new ArrayList<>();
    }

    public void ajouteConvive(String nom, int age){
        Personne convive = new Personne(nom, age);
        this.lesConvives.add(convive);
    }

    public double ageMoyen(){
        double somme = 0;
        for (Personne convive : this.lesConvives){
            somme += convive.getAge();        
        }
        return somme/this.lesConvives.size();
    }

    public int nombreDAdultes(){
        int compteur = 0;
        for(Personne pers : this.lesConvives)
        {
            if(pers.getAge() >= 18)
                compteur += 1;
        }
        return compteur;
    }

    public String lePlusJeune(){
        String nomPlusJeune = "";
        int agePlusJeune = this.lesConvives.get(0).getAge();
        for(int i = 1; i < this.lesConvives.size(); i++)
        {
            Personne conviveActuel = this.lesConvives.get(i);
            if (conviveActuel.getAge() < agePlusJeune){
                agePlusJeune = conviveActuel.getAge();
                nomPlusJeune = conviveActuel.getNom();
            }
        }
        return nomPlusJeune;
    }

    public boolean sontACote(String personne1, String personne2){
        for(int i = 1; i < this.lesConvives.size(); i++){
            if(this.lesConvives.get(i).getNom() == personne1 || this.lesConvives.get(i).getNom() == personne2){
                if(this.lesConvives.get(i-1).getNom() == personne1 || this.lesConvives.get(i-1).getNom() == personne2){
                    return true;
                }
            }
        }
        return false;
    }

    public void echange(String personne1, String personne2){
        int posPersonne1 = 0;
        int posPersonne2 = 0;
        for(int i = 0; i < this.lesConvives.size(); i++){
            if(this.lesConvives.get(i).getNom() == personne1){
                posPersonne1 = i;
            }
            else if(this.lesConvives.get(i).getNom() == personne2){
                posPersonne2 = i;
            }
        }
        Personne temp = this.lesConvives.get(posPersonne1);
        this.lesConvives.set(posPersonne1, this.lesConvives.get(posPersonne2));
        this.lesConvives.set(posPersonne2, temp);
    }

}
