import java.util.ArrayList;
import java.util.List;

public class Ville{

    private String nom;
    private List<Magasin> magasins;

    public Ville(String nom){
        this.nom = nom;
        this.magasins = new ArrayList<>();
    }

    public void ajouteMagasin(String nom,boolean lundi,boolean dimanche){
        this.magasins.add(new Magasin(nom,lundi,dimanche));
    }

    public List<Magasin> ouvertsLeLundi(){
        List<Magasin> resultat = new ArrayList<>();
        for(Magasin magasin : this.magasins){
            if(magasin.ouvertLundi())
                resultat.add(magasin);
        }
        return resultat;
    }

    @Override
    public String toString(){
        String resultat = "";
        for (Magasin magasin : this.magasins){
            resultat += magasin.toString();
            resultat += "\n";
        }
        return resultat;
    }

}
