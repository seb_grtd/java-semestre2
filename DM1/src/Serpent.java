public class Serpent extends Animal {
    private boolean venimeux;

    /**
     * Constructeur de la classe Serpent
     * @param nom Le nom du serpent
     * @param poids Le poids du serpent
     * @param venimeux Si le serpent est venimeux
     */
    public Serpent(String nom, double poids, boolean venimeux) {
        super(nom, poids);
        super.setSon("sssssssss");
        this.venimeux = venimeux;
    }

    /**
     * Affiche le serpent
     * @return String La chaîne de caractères représentant le serpent
     */
    @Override
    public String toString() {
        return super.toString() + (this.venimeux ? " et est venimeux" : " et n'est pas venimeux");
    }
}
