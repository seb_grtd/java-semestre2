import java.util.Comparator;

public class ComparateurAnimaux implements Comparator<Animal> {
    /**
     * Compare deux animaux en fonction de leur nom
     * @param a1 Le premier animal
     * @param a2 Le second animal
     * @return int La valeur de la comparaison
     */
    @Override
    public int compare(Animal a1, Animal a2) {
        return a1.getNom().compareTo(a2.getNom());
    }
}