public class Lion extends Animal {
    private boolean criniere;

    /**
     * Constructeur de la classe Lion
     * @param nom Le nom du lion
     * @param poids Le poids du lion
     * @param criniere Si le lion possède une crinière
     */
    public Lion(String nom, double poids, boolean criniere) {
        super(nom, poids);
        super.setSon("Rooaaaaaaar");
        this.criniere = criniere;
    }

    /**
     * Affiche le lion
     * @return String La chaîne de caractères représentant le lion
     */
    @Override
    public String toString() {
        return super.toString() + (this.criniere ? " et possède une crinière" : " et n'a pas de crinière");
    }
}
