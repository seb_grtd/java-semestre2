import java.util.List;
import java.util.ArrayList;

public class Enclos {
    private String nom;
    private double superficie;
    
    private boolean contientLions;

    private List<Animal> Animaux;

    /**
     * Constructeur de la classe Enclos
     * @param nom Le nom de l'enclos
     * @param superficie La superficie de l'enclos
     * @param contientLions Si l'enclos contient des lions
     */
    public Enclos(String nom, double superficie, boolean contientLions) {
        this.nom = nom;
        this.superficie = superficie;
        this.Animaux = new ArrayList<Animal>();
        this.contientLions = contientLions;
    }

    /**
     * Ajoute un animal à l'enclos
     * @param animal L'animal à ajouter
     */
    public void ajouterAnimal(Animal animal) {
        if (this.contientLions && animal instanceof Lion || !this.contientLions && animal instanceof Serpent){
            this.Animaux.add(animal);
        }
    }

    /**
     * Obtient la liste des animaux de l'enclos
     * @return List<Animal> La liste des animaux de l'enclos
     */
    public List<Animal> getAnimaux() {
        return this.Animaux;
    }

    /**
     * Obtient le nom de l'enclos
     * @return String Le nom de l'enclos
     */
    public String getNom() {
        return nom;
    }

    /**
     * Affiche l'enclos
     * @return String La chaîne de caractères représentant l'enclos
     */
    @Override
    public String toString() {
        String template = "Dans " + this.nom + "(" + this.superficie + " m2) ";
        String resultat = "";
        for (Animal a : this.getAnimaux()){
            resultat += template + a.toString() + ",\n";
        }
        return resultat;
    }
}
