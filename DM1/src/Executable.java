public class Executable {
    public static void main(String[] args) {
        
        Zoo zoo = new Zoo("Zoo de Beauval");
        
        Enclos enclosSerpent = new Enclos("Enclos des serpents", 100.0, false);
        Enclos enclosLion = new Enclos("Enclos des lions", 100.0, true);
        
        Serpent kaa = new Serpent("Kaa", 30.0, true);
        
        Lion simba = new Lion("Simba", 55.0, true);
        Lion mufasa = new Lion("Mufasa", 120.0, true);
        
        enclosSerpent.ajouterAnimal(kaa);
        enclosSerpent.ajouterAnimal(simba);
        
        enclosLion.ajouterAnimal(simba);
        enclosLion.ajouterAnimal(mufasa);
        enclosLion.ajouterAnimal(kaa);
        
        
        kaa.setEstBlesse(true);
        mufasa.setEstBlesse(true);
        simba.setEstBlesse(true);
        

        zoo.ajouterEnclos(enclosSerpent);
        zoo.ajouterEnclos(enclosLion);
        
        System.out.println("Test de la méthode soignerAnimal() de Zoo : \n");

        zoo.soignerAnimal("Mufasa");
        zoo.soignerAnimal("Jordan Lavenant");
        zoo.soignerAnimal("Mufasa");

        System.out.println("\nTest de la méthode toString() de Zoo : \n");

        System.out.println(zoo.toString());
        
        System.out.println("\nTest de la méthode listerAnimauxDansEnclos() de Zoo : \n");

        System.out.println(zoo.listerAnimauxDansEnclos("Enclos des lions"));

        System.out.println("----");

        System.out.println(zoo.listerAnimauxDansEnclos("Enclos des Jordan Lavenants"));

        System.out.println("\nTest de la méthode afficherDansOrdre() de Zoo :\n");
        
        System.out.println(zoo.afficherDansOrdre(new ComparateurAnimaux()));
    }

}
