import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Zoo {
    private String nom;
    private List<Enclos> lesEnclos;

    /**
     * Constructeur de la classe Zoo
     * @param nom Le nom du zoo
     */
    public Zoo(String nom) {
        this.nom = nom;
        this.lesEnclos = new ArrayList<Enclos>();
    }

    /**
     * Ajoute un enclos au zoo
     * @param enclos L'enclos à ajouter
     */
    public void ajouterEnclos(Enclos enclos) {
        this.lesEnclos.add(enclos);
    }

    /**
     * Soigne un animal
     * @param nomAnimal Le nom de l'animal à soigner
     */
    public void soignerAnimal(String nomAnimal) {
        for (Enclos e : this.lesEnclos){
            for (Animal a : e.getAnimaux()){
                if (a.getNom() == nomAnimal){
                    if (a.getEstBlesse()){
                        a.setEstBlesse(false);
                    }
                    else{
                        System.out.println("L'animal " + a.getNom() + " de l'enclos " + e.getNom() + " n'était pas blessé lorsque vous avez voulu le soigner.");
                    }
                    return;
                }
            }
        }
        System.out.println("L'animal " + nomAnimal + " n'a été trouvé dans aucuns des enclos du zoo.");
    }

    /**
     * Liste les animaux d'un enclos du Zoo
     * @param nomEnclos Le nom de l'enclos
     * @return La liste des animaux de l'enclos
     */
    public List<Animal> listerAnimauxDansEnclos(String nomEnclos){
        try{
            for (Enclos e : this.lesEnclos){
                if (e.getNom() == nomEnclos){
                    return e.getAnimaux();
                }
            }
            throw new Exception("La liste était vide");
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return new ArrayList<Animal>();
    }

    /**
     * Affiche les animaux du zoo
     * @return String La chaîne de caractères représentant le zoo
     */
    @Override
    public String toString() {
        String resultat = "Le " + this.nom + " contient \n[";
        for (Enclos e : this.lesEnclos){
            resultat += e.toString();
        }
        resultat = resultat.substring(0, resultat.length()-2);
        return resultat + "]";
    }

    /**
     * Affiche les animaux du zoo dans l'ordre spécifié par le comparateur
     * @param comparateur Le comparateur à utiliser
     */
    public String afficherDansOrdre(Comparator<Animal> comparateur){
        List<Animal> animauxTries = new ArrayList<Animal>();

        for (Enclos e : this.lesEnclos){
            for (Animal a : e.getAnimaux()){
                animauxTries.add(a);
            }
        }

        Collections.sort(animauxTries, comparateur);

        String resultat = "Le " + this.nom + " contient les animaux suivants (dans l'ordre alphabétique de leur nom): \n[";

        for (Animal a : animauxTries){
            resultat += a.toString() + ",\n";
        }

        resultat = resultat.substring(0, resultat.length()-2);

        return resultat + "]";
    }
}
