public class Animal{
    private String nom;
    private double poids;
    private boolean estBlesse;
    private String son;

    /**
     * Constructeur de la classe Animal
     * @param nom Le nom de l'animal
     * @param poids Le poids de l'animal
     */
    public Animal(String nom, double poids) {
        this.nom = nom;
        this.poids = poids;
        this.estBlesse = false;
        this.son = "";
    }

    /**
     * Définit le son de l'animal
     * @param son Le son de l'animal
     */
    public void setSon(String son) {
        this.son = son;
    }

    /**
     * Obtient le nom de l'animal
     * @return String Le nom de l'animal
     */
    public String getNom() {
        return this.nom;
    }

    /**
     * Définit si l'animal est blessé
     * @param estBlesse Si l'animal est blessé
     */
    public void setEstBlesse(boolean estBlesse) {
        this.estBlesse = estBlesse;
    }

    /**
     * Obtient si l'animal est blessé
     * @param estBlesse Si l'animal est blessé
     */
    public boolean getEstBlesse(){
        return this.estBlesse;
    }

    /**
     * Affiche l'animal
     * @return String La chaîne de caractères représentant l'animal
     */
    @Override
    public String toString() {
        return nom + ", " + (estBlesse ? "blessé" : "non blessé")+ ", pèse " + poids + " kg";
    }
}