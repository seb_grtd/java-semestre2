public class Vaisseau{

    /** le nom du vaisseau **/
    private String nom;

    /** le nombre de passagers du vaisseau **/
    private int nombreDePassagers;

    /** la puissance de feu du vaisseau **/
    private int puissanceDeFeu;

    /** Construit un vaisseau sans passagers avec une puissance et un nom donné **/
    public Vaisseau(String nom, int puissance){
        this.nom=nom;
        this.puissanceDeFeu = puissance;
        this.nombreDePassagers =0;
    }

    /** Construit un vaisseau avec un nombre de passagers, un nom et une puissance donnée **/
    public Vaisseau(String nom, int puissance, int passagers){
        this.nom=nom;
        this.puissanceDeFeu = puissance;
        this.nombreDePassagers = passagers;
    }

    /** Permet de connaître le nom du vaisseau **/
    public String getNom(){
        return this.nom;
    }

    /** Permet de connaître le nombre de passagers dans le vaisseau **/
    public int getNombrePassagers(){
        return this.nombreDePassagers;
    }

    /** Permet de connaître la puissance du vaisseau **/
    public int getPuissance(){
        return this.puissanceDeFeu;
    }

    /** Permet de savoir si un vaisseau transporte des passagers ou non **/
    public boolean transportePassagers(){
        return this.nombreDePassagers != 0;
    }


}