import java.util.List;
import java.util.ArrayList;

public class Flotte{
    /** Nom de la flotte **/
    private String nom;
    /** Liste des vaisseaux de la flotte **/
    private List<Vaisseau> vaisseaux;

    /** Construit une flotte vide, avec comme nom par défaut "Nouvelle Flotte" **/
    public Flotte(){
        this.nom = "Nouvelle Flotte";
        this.vaisseaux = new ArrayList<>();
    }

    /** Construit une nouvelle flotte vide, avec le nom passé en paramètre **/
    public Flotte(String nom){
        this.nom = nom;
        this.vaisseaux = new ArrayList<>();
    }

    /** Permet d'ajouter un vaisseau à partir de l'objet lui même à la flotte **/
    public void ajoute(Vaisseau vaisseau){
        this.vaisseaux.add(vaisseau);
    }

    /** Permet d'ajouter un nouveau vaisseau à la flotte en précisant son nom et sa puissance **/
    public void ajoute(String nom, int puissance){
        this.vaisseaux.add(new Vaisseau(nom, puissance));
    }

    /** Permet d'ajouter un nouveau vaisseau à la flotte en précisant son nom, sa puissance et son nombre de passagers**/
    public void ajoute(String nom, int puissance, int nombreDePassagers){
        this.vaisseaux.add(new Vaisseau(nom, puissance, nombreDePassagers));
    }

    /** Permet de connaître le nombre de vaisseaux dans la flotte **/
    public int nombreVaisseaux(){
        return this.vaisseaux.size();
    }

    /** Permet de compter le nombre de vaisseaux sans passagers **/
    public int nombreDeVaisseauxSansPassagers(){
        int compteur = 0;
        for (Vaisseau vaisseau : this.vaisseaux){
            if(!vaisseau.transportePassagers())
                compteur += 1;
        }
        return compteur;
    }


    /** Permet de connaitre le nom du vaisseau le moins puissant **/
    public String nomDuVaisseauLeMoinsPuissant(){
        String nomMin = this.vaisseaux.get(0).getNom();
        int puissanceMin = this.vaisseaux.get(0).getPuissance();

        for(Vaisseau vaisseau : this.vaisseaux){
            if(vaisseau.getPuissance() < puissanceMin){
                nomMin = vaisseau.getNom();
                puissanceMin = vaisseau.getPuissance();
            }
        }
        return nomMin;
    }

    /** Permet de connaître la puissance du vaisseau le plus puissant de la flotte **/

    public int puissanceDeFeuMax(){
        int puissanceMax = this.vaisseaux.get(0).getPuissance();
        
        for (Vaisseau vaisseau : this.vaisseaux){
            if(vaisseau.getPuissance() > puissanceMax)
                puissanceMax = vaisseau.getPuissance();
        }
        return puissanceMax;
    }

    /** Permet de connaître la puissance totale de la flotte **/
    public int totalPuissance(){
        int compteur = 0;
        for (Vaisseau vaisseau : this.vaisseaux){
            compteur += vaisseau.getPuissance();
        }
        return compteur;
    }

    /** Permet de connaître le nom de la flotte actuelle **/
    public String getNom(){
        return this.nom;
    }

}