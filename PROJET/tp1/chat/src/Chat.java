public class Chat{
/**Nomduchat*/
private String nom;
/**Cetattributmodéliseàquelpointlechatestbavard*/
private int bavard;
/**permetdeconstruireunChatplusoumoinsbavard*/
public Chat(String nomDuChat,int bavard){
this.nom=nomDuChat;
this.bavard=bavard;
}
/**permetdeconstruireunChat"normal"*/
public Chat(String nomDuChat){
this(nomDuChat,1);
}
public String getNom(){
return this.nom;
}
public void setNom(String nouveauNom){
this.nom=nouveauNom;
}
public void devientMuet(){
this.bavard=0;
}
/**
*Cetteméthodefaitmiaulerlechatenaffichantunmessage
*surlasortiestandard(dansleterminalpardéfaut)
*Pluslechatestbavard,etplusilmiaule
*/
public void miaule(){
System.out.print(this.nom);
for(int i=0;i<this.bavard;i++){
System.out.print("Miaou!");
}
System.out.println("...");
}
/**
*Cetteméthodedéterminesilechatestendormi
*@paramheureundoublecomprisentre0et24
*@returntruesilechatdort(presquetoutletemps)
*etfalsesinon(c’estàdireentre3et4heuresdumatin)
*/
public boolean estEndormi(double heure){
return (heure<=3||heure>4);
}
}