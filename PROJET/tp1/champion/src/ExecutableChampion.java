public class ExecutableChampion{
    public static void main(String[]args){
        /*Créationdesinstances*/
        Champion teemo=new Champion("Teemo",37,7,0);
        Champion darius=new Champion("Darius",38,11,0);
        Champion sona= new Champion("Sona", 27, 3, 5);
        System.out.println(sona.toString());
        //Sona:pv=27,attaque=3,soin=5
        teemo.combat(darius);
        assert teemo.getPointsDeVie() == 26;
        assert darius.getPointsDeVie() == 31;
        teemo.boitUnePotionDeSoin();
        assert teemo.getPointsDeVie() == 31;
        System.out.println("premiercombat");
        System.out.println(darius.toString());
        System.out.println(teemo.toString());
        while(teemo.estEnVie()&&darius.estEnVie()){
            sona.soigne(teemo);
            teemo.combat(darius);
        }
        System.out.println("finducombat");
        System.out.println(darius.toString());
        System.out.println(teemo.toString());
    }
}