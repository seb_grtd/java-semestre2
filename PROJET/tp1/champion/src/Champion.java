public class Champion{
    private String nom;
    private int pointsDeVie;
    private int attaque;
    private int soin;

    public Champion(String nom, int pointsDeVie, int attaque, int soin){
        this.nom = nom;
        this.pointsDeVie = pointsDeVie;
        this.attaque = attaque;
        this.soin = soin;
    }

    public void combat(Champion adversaire){
        this.pointsDeVie -= adversaire.attaque;
        adversaire.pointsDeVie -= this.attaque;
    }

    public int getPointsDeVie(){
        return this.pointsDeVie;
    }

    public void boitUnePotionDeSoin(){
        this.pointsDeVie += 5;
    }

    public void soigne(Champion championBlesse){
        championBlesse.pointsDeVie += this.soin;
    }

    public boolean estEnVie(){
        return this.pointsDeVie > 0;
    }

    @Override
    public String toString(){
        return this.nom+" : pv="+this.pointsDeVie+", attaque="+this.attaque+", soin="+this.soin;
    }
}