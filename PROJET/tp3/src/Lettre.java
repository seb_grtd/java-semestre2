import java.util.Arrays;
import java.util.List;

public class Lettre{
    private static List<String> alphabetMorse = Arrays.asList("=_===", 
            "===_=_=_=", "===_===_=", "===_=_=", "=", "=_=_===_=",
            "===_===_=", "=_=_=_=", "=_=", "=_===_===_===", "===_=_===",
            "=_===_=_=", "===_===", "===_=", "===_===_===", "=_===_===_=",
            "===_===_=_===", "=_===_=", "=_=_=", "===", "=_=_===",
            "=_=_=_===", "=_===_===", "===_=_=_===", "===_=_===_===",
            "===_===_=_=", "_______");
    private static String alphabet = "ABCDEFHHIJKLMNOPQRSTUVWXYZ ";
    private char lettre;

    /** Constructeur prenant en paramètre une lettre sous la forme d'un caractère. **/
    public Lettre(char lettre){
        this.lettre = Character.toUpperCase(lettre);    
    }

    /** Constructeur prenant en paramètre une chaîne de caractères correspondant à une lettre en morse. **/
    public Lettre(String morse){
        /** il y a le même nombre de lettres dans nos deux alphabets, donc on fait un parcours par indice de l'alphabet morse
         * et s'il y a correspondance, notre lettre correspond à la lettre à l'indice en question dans l'alphabet
         */
        for(int i = 0; i < alphabetMorse.size(); i++){
            if (alphabetMorse.get(i).equals(morse)){
                this.lettre = alphabet.charAt(i);
            }
        }
    }

    /** Cherche le numéro de la lettre (son indice dans l'alphabet).**/
    public int toNumero(){
        for(int i = 0; i < alphabet.length(); i++){
            char c = alphabet.charAt(i);
            if (c == this.lettre){
                return i;
            }
        }
        return -1;
    }

    /** Retourne notre lettre. **/
    public char toChar(){
        return this.lettre;
    }

    /** Retourne l'équivalent de notre lettre en morse. **/
    public String toMorse(){
        return alphabetMorse.get(this.toNumero());
    }

    /** Renvoie notre lettre sous la forme d'un String.**/
    @Override
    public String toString() {
        return String.valueOf(lettre);
    }  

    /** Renvoie si deux lettres sont les mêmes. **/
    @Override
    public boolean equals(Object obj) {
        if(obj == null){return false;}
        if(obj == this){return true;}
        if (obj instanceof Lettre){
            Lettre b = (Lettre) obj;
            return b.toChar() == this.toChar();
        }
        return false;
    }
}