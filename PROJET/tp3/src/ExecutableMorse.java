public class ExecutableMorse{
    public static void main(String[] args){
        Lettre n = new Lettre('N');
        assert n.toNumero() == 13;
        assert n.toChar() == 'N';
        assert n.toMorse().equals("===_=");
        assert n.toString().equals("N");
        Lettre a = new Lettre("=_===");
        assert a.toNumero() == 0;
        assert a.toMorse().equals("=_===");
        assert a.toChar() == 'A';
        assert a.toString().equals("A");
        Lettre espace = new Lettre(' ');
        assert espace.toNumero() == 26;
        assert espace.toMorse().equals("_______");
        assert espace.toChar() == ' ';
        assert espace.toString().equals(" ");

        //tests pour texte

        Texte t = new Texte("Le Texte");
        assert t.toString().equals("LE TEXTE");
        assert t.toMorse().equals("=_===_=_=___=_______===___=___===_=_=_===___===___=");
        assert t.contient(new Lettre('e'));
        assert t.contient(new Lettre('E'));
        assert !t.contient(new Lettre('P'));

        assert Texte.decode(t.toMorse()).equals("LE TEXTE");
        assert Texte.decode(new Texte("Super").toMorse()).equals("SUPER");
        assert Texte.decode(new Texte("").toMorse()).equals("");
        assert Texte.decode(new Texte(" ").toMorse()).equals("");
        assert Texte.decode(new Texte("Super  ").toMorse()).equals("SUPER");
    
        //t.toSon();

        new Texte("j ai beaucoup aime ce tp").toSon();
    }
}