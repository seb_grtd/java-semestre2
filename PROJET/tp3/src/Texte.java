import java.util.ArrayList;
import java.util.List;

public class Texte {
    public List<Lettre> leTexte;

    /** Constructeur auquel on passe en paramètre un texte écrit avec les 26 lettres de l'alphabet ou un espace. PS: les lettres deviennent toutes des majuscules. **/
    public Texte(String texteToConvert){
        this.leTexte = new ArrayList<>();
        /** pour chaque caractère de notre texte, on l'ajoute à notre liste de lettres "leTexte".  */
        for (int i = 0; i < texteToConvert.length(); i++){
            this.leTexte.add(new Lettre(Character.toUpperCase(texteToConvert.charAt(i))));
        }
    }

    /** Renvoie le texte sous la forme d'un String où les caractères sont en majuscule! **/
    @Override
    public String toString(){
        String res = "";
        for (Lettre l : this.leTexte){
            res += l.toString();
        }
        return res;
    }

    /** Renvoie le texte sous la forme d'une chaîne de caractères où toutes nos lettres ont été codées en morse.**/
    public String toMorse(){
        String res = "";
        /** Pour chaque lettre du texte */
        for (int i = 0; i < this.leTexte.size()-1 ; i++){
            res += this.leTexte.get(i).toMorse();
            /** On ajoute son équivalent morse au résultat */
            if(' ' != this.leTexte.get(i+1).toChar() && ' ' != this.leTexte.get(i).toChar()){
                /** Si le prochain caractère n'est pas un espace et que le caractère actuel non plus, on rajoute trois tirets du bas
                 * pour marquer la fin de notre lettre.
                 */
                res += "___";
            }
            if(i+1 == this.leTexte.size()-1){
                /** si la prochaine lettre est la dernière lettre, nous l'ajoutons directement à ce tour de boucle
                 * (sinon out of range pour la ligne 33 au tour de boucle suivant)
                 */
                res += this.leTexte.get(i+1).toMorse();
            }
        }
        return res;
    }

    /** Renvoie si une lettre est contenue dans le texte **/
    public boolean contient(Lettre lettre){
        return leTexte.contains(lettre);
    }

    /** Permet de décoder une chaîne de caractère représentant des lettres codées morse. **/
    public static String decode(String texteEnMorse){
        String texteDecode = "";
        if (texteEnMorse == ""){
            return texteEnMorse;
        }
        String[] splString = texteEnMorse.split("_______");
        /** on sépare chaque mot dans une liste de String. */
        for(int i = 0; i < splString.length ; i++)
        {
            String[] lettresMorse = splString[i].split("___");
            /** on sépare chaque lettre de notre mot dans une autre liste. */
            for(String lettreMorse : lettresMorse){
                /** pour chaque lettre, on ajoute son équivalent sous la forme d'un caractère dans notre résultat. */
                texteDecode += new Lettre(lettreMorse).toChar();
            }
            if (i < splString.length-1)
            /**on ajoute un espace entre chaque mot (sauf si ce mot est le dernier mot).*/
                texteDecode += " ";
        }
        return texteDecode;
    }

    /** Joue un son correspondant au code morse de notre texte. **/
    public void toSon(){
        Son r = new Son();
        String morse = this.toMorse();
        int i = 0;
        System.out.println(morse);
        while(i < morse.length()){
            if (morse.charAt(i) == '_'){
               r.pause();
            }
            else{
                if (i+2 < morse.length()){
                    if(morse.charAt(i+1) == '=' && morse.charAt(i+2) == '='){
                        r.tone(300);
                        i+=2;
                    }
                    else{
                        r.tone(100);
                    }
                }   
                else{
                    r.tone(100);
                }
            }
            i+=1;
        }
    }

}
