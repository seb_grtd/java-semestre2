import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Achats {
    private HashMap<Produit, Integer> achats;

    public Achats(){
        this.achats = new HashMap<Produit, Integer>();
    }

    public void ajouterProduit(Produit p, int qte){
        if (!achats.containsKey(p))
            this.achats.put(p, qte);
        else
            this.achats.put(p, achats.get(p)+qte);
    }

    public double factureTotale(){
        double total = 0;
        for (Produit p : achats.keySet()){
            total += p.getPrixProduit()*achats.get(p);
        }
        return total;
    }

    public List<Produit> produitsTrieParNomProd(){
        List<Produit> res = new ArrayList<Produit>(achats.keySet());
        Collections.sort(res, new ComparateurNomProduit());
        return res;
    }

    public List<Produit> produitsTriesParPrix(){
        List<Produit> res = new ArrayList<Produit>(achats.keySet());
        Collections.sort(res, new ComparateurPrixProduit());
        return res;
    }

    @Override
    public String toString() {
        String resultat = "";
        for (Produit p : achats.keySet()){
            resultat += "Produit: "+p.toString()+" qte: "+achats.get(p)+"\n";
        }
        // on enlève le dernier caractère (un espace)
        return resultat.substring(0, resultat.length()-1);
    }

}
