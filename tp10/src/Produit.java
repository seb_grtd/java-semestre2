public class Produit {
    private String nomProduit;
    private double prixProduit;

    public Produit(String n, double p){
        this.nomProduit =n;
        this.prixProduit =p;
    }

    @Override
    public String toString(){
        return nomProduit+" pour "+prixProduit+" euro(s)";
    }

    public double getPrixProduit() {
        return prixProduit;
    }

    public String getNomProduit() {
        return nomProduit;
    }
}
