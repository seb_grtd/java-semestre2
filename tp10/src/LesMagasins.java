import java.util.Set;
import java.util.HashSet;

public class LesMagasins {
    private Set<Magasin> ensembleMagasins;

    public LesMagasins(){
        this.ensembleMagasins = new HashSet<Magasin>();
    }

    public void ajouterMagasin(Magasin magasin){
        this.ensembleMagasins.add(magasin);
    }

    public Set<Magasin> getMagasinsOuvertsLeLundi(){
        Set<Magasin> magasinsOuvertsLeLundi = new HashSet<Magasin>();

        for(Magasin magasin : this.ensembleMagasins){
            if(magasin.ouvertLundi()){
                magasinsOuvertsLeLundi.add(magasin);
            }
        }

        return magasinsOuvertsLeLundi;
    }

    
}
