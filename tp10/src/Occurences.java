import java.util.HashMap;
import java.util.List;

public class Occurences {
    private HashMap<Integer, Integer> dicoOccurences;

    public Occurences(List<Integer> listeEntiers){
        dicoOccurences = new HashMap<Integer, Integer>();
        for (Integer entier : listeEntiers){
            if (dicoOccurences.containsKey(entier)){
                dicoOccurences.put(entier, dicoOccurences.get(entier)+1);
            }
            else{
                dicoOccurences.put(entier, 1);
            }
        }
    }

    @Override
    public String toString() {
        return "Occurences [dicoOccurences=" + dicoOccurences + "]";
    }

}
