import java.util.ArrayList;
import java.util.List;

public class ExecOccurence {
    public static void main(String[] args){
        List<Integer> listeEntiers = new ArrayList<>();

        listeEntiers.add(1);
        listeEntiers.add(6);
        listeEntiers.add(8);
        listeEntiers.add(24);
        listeEntiers.add(5);
        listeEntiers.add(32);
        listeEntiers.add(6);
        listeEntiers.add(6);

        System.out.println("La liste contient " + listeEntiers.size() + " éléments");
        
        System.out.println(new Occurences(listeEntiers));
        
    }
}
