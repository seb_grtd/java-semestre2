import java.util.Comparator;

public class ComparateurPrixProduit implements Comparator<Produit> {
    @Override
    public int compare(Produit p1, Produit p2) {
        if (p1.getPrixProduit() < p2.getPrixProduit())
            return -1;
        else if (p1.getPrixProduit() > p2.getPrixProduit())
            return 1;
        else
            return 0;
    }
}
