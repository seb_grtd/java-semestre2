public class Magasin{

    private String nom;
    private boolean ouvertLundi;
    private boolean ouvertDimanche;

    public Magasin(String nom,boolean lundi,boolean dimanche){
        this.nom = nom;
        this.ouvertLundi = lundi;
        this.ouvertDimanche = dimanche;
    }

    public String getNom(){
        return this.nom;
    }

    public boolean ouvertLundi(){
        return this.ouvertLundi;
    }

    public boolean ouvertDimanche(){
        return this.ouvertDimanche;
    }

    @Override
    
    public String toString(){
        return "Magasin: "+this.getNom()+" ouvert lundi: "+this.ouvertLundi()+" ouvert dimanche: "+this.ouvertDimanche();
    }

    @Override
    public boolean equals(Object obj){
        if(obj == null){
            return false;
        }
        if(obj == this){
            return true;
        }
        if((obj instanceof Magasin)){
            Magasin magasin = (Magasin) obj;
            return this.getNom().equals(magasin.getNom()) && this.ouvertLundi() == magasin.ouvertLundi() && this.ouvertDimanche() == magasin.ouvertDimanche();
    
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (this.nom != null ? this.nom.hashCode() : nom.length());
        hash = 127 * hash + (this.ouvertLundi ? 1 : 0);
        hash = 31 * hash + (this.ouvertDimanche ? 1 : 0);
        return hash;
    }
}