public class Executable {
    public static void main(String[] args) {
        Produit salade1 = new Produit("Salade", 1.5);
        Produit brocolis = new Produit("Brocolis", 2.75);
        System.out.println(salade1);

        Achats achats = new Achats();

        achats.ajouterProduit(brocolis, 2);
        achats.ajouterProduit(brocolis, 3);
        achats.ajouterProduit(salade1, 2);
        achats.ajouterProduit(new Produit("Saucisson", 2.7), 1);

        System.out.println("Affichage des achats:");
        System.out.println(achats);

        System.out.println("Affichage du prix total du panier:");
        System.out.println(achats.factureTotale());

        System.out.println("Affichage des achats triés par nom de produit:");
        System.out.println(achats.produitsTrieParNomProd());

        System.out.println("Affichage des achats triés par prix:");
        System.out.println(achats.produitsTriesParPrix());
    
    }
}
