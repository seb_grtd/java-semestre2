import java.util.Comparator;

public class ComparateurNomProduit implements Comparator<Produit>{
    
    @Override
    public int compare(Produit p1, Produit p2){
        if (p1.getNomProduit().compareTo(p2.getNomProduit()) > 0)
        {
            return 1;
        }
        else if (p1.getNomProduit().compareTo(p2.getNomProduit()) == 0){
            return 0;
        }
        return -1;
    }
}
