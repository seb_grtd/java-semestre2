import java.util.List;
import java.util.ArrayList;

import java.util.Set;
import java.util.HashSet;

public class ExecNumber{
    public static void main(String[] args){
        List<Number> tableau = new ArrayList<>();
        tableau.add(5);
        tableau.add(6.);
        tableau.add(7.f);
        Number number = 8.5f;
        tableau.add(number);

        System.out.println((int) tableau.stream().mapToDouble(Number::doubleValue).sum());

        System.out.println(tableau);

        Number x = new Integer(5);
        Number y = new Double(5);
        System.out.println(x.equals(y));

        LesMagasins lesMagasins = new LesMagasins();

        lesMagasins.ajouterMagasin(new Magasin("IGA", true, false));
        lesMagasins.ajouterMagasin(new Magasin("Metro", true, true));
        lesMagasins.ajouterMagasin(new Magasin("Maxi", false, true));
        lesMagasins.ajouterMagasin(new Magasin("Maxi", true, true));
        lesMagasins.ajouterMagasin(new Magasin("Maxi", false, true));
        lesMagasins.ajouterMagasin(new Magasin("Maxi", true, true));

        System.out.println(lesMagasins.getMagasinsOuvertsLeLundi());
    }
}