import java.util.List;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Collections;

public class BibComplexes {
    public BibComplexes(){}
    
    public static boolean estLeComplexeLePlusPetit(List<Complexe> listeComplexes, Comparator<Complexe> comparateur, Complexe c){
        List<Complexe> copieListeComplexes = new ArrayList<>(listeComplexes);
        Collections.sort(copieListeComplexes, comparateur);
        
        return copieListeComplexes.get(0).equals(c);
    }

    public static boolean estComprisEntre(List<Complexe> listeComplexes, Comparator<Complexe> comparateur, Complexe c1, Complexe c2){
        List<Complexe> copieListeComplexes = new ArrayList<>(listeComplexes);
        Collections.sort(copieListeComplexes, comparateur);

        return (comparateur.compare(copieListeComplexes.get(0), c1) >=0) && (comparateur.compare(c2, copieListeComplexes.get(copieListeComplexes.size()-1)) >=0); 
    }
}
