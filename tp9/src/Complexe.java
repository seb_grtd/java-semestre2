public class Complexe {
    private double reel;
    private double imaginaire;

    public Complexe(double reel, double imaginaire){
        this.reel = reel;
        this.imaginaire = imaginaire;
    }

    public double getReel(){
        return this.reel;
    }

    public double getImaginaire(){
        return this.imaginaire;
    }

    public double getNorme(){
        return Math.pow(this.getReel(), 2) + Math.pow(this.getImaginaire(),2);
    }

    @Override
    public boolean equals(Object obj){
        if (obj == null ) { return false; }
        if (obj == this ) { return true; }
        if (obj instanceof Complexe){
            Complexe c = (Complexe) obj; 
            return this.getReel()==c.getReel() && this.getImaginaire()==c.getImaginaire();
        }
        return false;
    }
}
