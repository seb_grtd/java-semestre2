import java.util.Comparator;

public class ComparerComplexe implements Comparator<Complexe> {
    public int compare(Complexe c1, Complexe c2) {
        if (c1.getReel() > c2.getReel()) {
            return 1;
        }
        else if (c1.getReel() == c2.getReel())
        {
            return 0;
        }
        return -1; 
    }
}
