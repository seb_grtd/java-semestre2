import java.util.List;

import javax.swing.event.SwingPropertyChangeSupport;

import java.util.ArrayList;

public class Executable{
    public static void main(String[] args){
        Personne p = new Personne("John", 25);
        System.out.println(p.getNom());
        System.out.println(p.getAge());

        List<Personne> personnes = new ArrayList<Personne>();
        personnes.add(new Personne("John", 21));
        personnes.add(new Personne("Jane", 36));
        personnes.add(new Personne("Jack", 35));
        personnes.add(new Personne("Jill", 49));
        personnes.add(new Personne("Jenny", 53));
        personnes.add(new Personne("Jilbert", 17));

        List<Personne> personnesTries = Bibliotheque.listePersonnesTriees(personnes);

        System.out.println("-- Affichage personnes triées");
        for(Personne personne : personnesTries){
            System.out.print("Prenom "+ personne.getNom() +" Age: ");
            System.out.print(personne.getAge() +"\n");
        }

        System.out.println("-- Affichage de l'ecart d'âge minimum");
        System.out.println(Bibliotheque.ecartAgeMinimum(personnes));
        assert(Bibliotheque.ecartAgeMinimum(personnes) == 25);

        List<Complexe> complexes = new ArrayList<Complexe>();
        complexes.add(new Complexe(7, 5));
        complexes.add(new Complexe(1, 2));
        complexes.add(new Complexe(9, 3));
        complexes.add(new Complexe(3, 8));
        complexes.add(new Complexe(11, 4));
        complexes.add(new Complexe(5, 0));
        
        List<Complexe> complexesTries = Bibliotheque.listeComplexeTriees(complexes);
        
        System.out.println("-- Affichage des complexes tries par ordre croissant des parties réelles");
        for(Complexe complexe : complexesTries){
            System.out.println("Partie réelle "+complexe.getReel()+" Partie imaginaire: "+complexe.getImaginaire());
        }

        List<Complexe> complexesTriesNorme = Bibliotheque.listeComplexeTrieesNorme(complexes);

        System.out.println("-- Affichage des complexes tries par norme croissante");
        for(Complexe complexe : complexesTriesNorme){
            System.out.println("Partie réelle "+complexe.getReel()+" Partie imaginaire: "+complexe.getImaginaire()+ " Norme: "+complexe.getNorme());
        }

        System.out.println("-- Affichage des complexes tries par norme decroissante");
        List<Complexe> complexesTriesNormeDecroissante = Bibliotheque.listeComplexeTrieesNormeDecroissante(complexes);
        
        for(Complexe complexe : complexesTriesNormeDecroissante){
            System.out.println("Partie réelle "+complexe.getReel()+" Partie imaginaire: "+complexe.getImaginaire()+ " Norme: "+complexe.getNorme());
        }
        
        Complexe complexeAvecPlusGrandeNorme = Bibliotheque.complexePlusGrandeNorme(complexes);

        System.out.println("-- Affichage du complexe avec la plus grande norme");
        System.out.println("Partie réelle "+complexeAvecPlusGrandeNorme.getReel()+" Partie imaginaire: "+complexeAvecPlusGrandeNorme.getImaginaire()+ " Norme: "+complexeAvecPlusGrandeNorme.getNorme());

        System.out.println("-- Test de la méthode estLeComplexeLePlusPetit");
        System.out.println("Selon le comparateur de parties réelles Complexe(1,2) est le plus petit: "+BibComplexes.estLeComplexeLePlusPetit(complexes, new ComparerComplexe(), new Complexe(1, 2)));
        System.out.println("Selon le comparateur de normes Complexe(1,2) est le plus petit: "+BibComplexes.estLeComplexeLePlusPetit(complexes, new ComparerComplexeNorme(), new Complexe(1, 2)));
    
        Complexe complexe1 = new Complexe(0, 2);
        Complexe complexe2 = new Complexe(2, 0);
        Complexe complexe3 = new Complexe(12, 4);

        System.out.println("-- Test de la méthode estComprisEntre");
        System.out.println("Selon le comparateur de parties réelles Complexe(1,2) est compris entre Complexe(0,2) et Complexe(2,0): "+BibComplexes.estComprisEntre(complexes, new ComparerComplexe(), complexe1, complexe2));
        System.out.println("Selon le comparateur de normes Complexe(1,2) est compris entre Complexe(0,2) et Complexe(11,4): "+BibComplexes.estComprisEntre(complexes, new ComparerComplexeNorme(), complexe1, complexe3));
    }
}