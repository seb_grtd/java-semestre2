import java.util.Comparator;

public class ComparerComplexeNorme implements Comparator<Complexe> {
    public int compare(Complexe c1, Complexe c2) {
        if (c1.getNorme() > c2.getNorme()) {
            return 1;
        }
        else if (c1.getNorme() == c2.getNorme()){
            return 0;
        }
        return -1; 
    }
}
