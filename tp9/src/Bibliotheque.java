import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class Bibliotheque {
    public Bibliotheque(){}

    public static List<Personne> listePersonnesTriees(List<Personne> personnes){
        List<Personne> copiePersonnes = personnes;
        Collections.sort(copiePersonnes);
        return copiePersonnes;
    }

    public static int ecartAgeMinimum(List<Personne> personnes){
        List<Personne> personnesTries = listePersonnesTriees(personnes);
        int ecartMinimum = -1;
        for(int i = 1; i < personnesTries.size() - 1; i++){
            for(int j = i + 1; j < personnesTries.size(); j++){
                int ecart = Math.abs(personnesTries.get(i).compareTo(personnesTries.get(j)));
                if(ecartMinimum < 0 || ecart < ecartMinimum){
                    ecartMinimum = ecart;
                }
            }
        }

        return ecartMinimum;
    }

    public static List<Complexe> listeComplexeTriees(List<Complexe> complexes){
        List<Complexe> copieComplexe = complexes;
        Collections.sort(copieComplexe, new ComparerComplexe());
        return copieComplexe;
    }

    public static List<Complexe> listeComplexeTrieesNorme(List<Complexe> complexes){
        List<Complexe> copieComplexe = complexes;
        Collections.sort(copieComplexe, new ComparerComplexeNorme());
        return copieComplexe;
    }

    public static List<Complexe> listeComplexeTrieesNormeDecroissante(List<Complexe> complexes){
        List<Complexe> copieComplexe = complexes;
        Collections.sort(copieComplexe, new ComparerComplexeNorme());
        Collections.reverse(copieComplexe);
        return copieComplexe;
    }

    public static Complexe complexePlusGrandeNorme(List<Complexe> complexes){
        List<Complexe> complexesTries = listeComplexeTrieesNormeDecroissante(complexes);
        return complexesTries.get(0);
    }
}
